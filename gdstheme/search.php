<?php get_header(); ?>

	<?php get_template_part('inc/modules/content', 'title'); ?>

	<div class="content-container">
		<div class="row">
			<div class="large-12 columns">
				<h2>
					Search Result for:
					<?php // Calculate # of results matching search term
						$allsearch = new WP_Query(
							"s=$s&showposts=-1");
							$key = wp_specialchars($s, 1);
							$count = $allsearch->post_count; _e(''); _e('<span class="search-terms">');
							echo $key; _e('</span>'); _e(' &mdash; ');
							echo $count . ' '; _e('articles');
						wp_reset_query();
					?>
				</h2>

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<div class="row">
					<div class="large-12 columns">
						<div class="row">
							<div class="large-12 columns">
								<h3 class="post-title">
									<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
								</h3>
							</div>
						</div>
						<div class="row">
							<div class="large-12 columns">
								<span class="excerpt">
									<?php search_excerpt_highlight(); ?>
								</span><!-- /.excerpt -->
							</div>
						</div>
					</div>
				</div>
				<?php endwhile; ?>

				<?php else : ?>

				<h2>Sorry Nothing Found</h2>

				<?php endif; ?>
			</div>
			<?php get_sidebar('right'); ?>
			<?php get_template_part('inc/acf/page', 'builder'); ?>
		</div>
	</div>
<?php get_footer(); ?>
