<div id="sidebar">

	<h2>
		Services Provided
		<?php //$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ); echo $term->name; ?>
	</h2>

	<?php
		$terms = get_field('services_provided');
		if( $terms ): ?>

		<ul>
		<?php foreach( $terms as $term ): ?>
			<li><?php echo $term->name; ?></li>
		<?php endforeach; ?>
		</ul>

	<?php endif; ?>

</div><!-- /#sidebar -->