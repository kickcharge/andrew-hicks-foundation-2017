<div class="medium-4 hide-for-small-only columns">

<?php if (is_search() ) : ?>

	<?php get_template_part('inc/modules/sidebars/content', 'search'); ?>

<?php elseif(!is_single() ) : ?>

	<?php get_template_part('inc/modules/sidebars/content', 'page'); ?>

<?php elseif (is_singular('case-studies') ) : ?>

	<?php get_template_part('inc/modules/sidebars/content', 'case_studies'); ?>

<?php elseif (is_singular('portfolio') ) : ?>

	<?php get_template_part('inc/modules/sidebars/content', 'portfolio'); ?>

<?php elseif (is_singular('products') ) : ?>

	<?php get_template_part('inc/modules/sidebars/content', 'products'); ?>

<?php elseif (is_single() ) : ?>

	<?php get_template_part('inc/modules/sidebars/content', 'single'); ?>

<?php else : endif; ?>

</div>
