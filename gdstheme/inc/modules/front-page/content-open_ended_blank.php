<?php
	$field = get_sub_field_object( 'open_ended_content' );
?>

<section class="open_ended_content" data-field="<?php echo $field['key']; ?>">
	
	<div class="row">
	<?php the_sub_field('open_ended_content'); ?>
	</div><!--end of row-->

</section>
