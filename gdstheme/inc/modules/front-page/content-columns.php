<?php $field = get_sub_field_object( 'column_builder' ); ?>

      <section class="statistics section clearfix" data-field="<?php echo $field['key']; ?>">
        <div class="wrap">

			<h1><?php the_sub_field('section_title_columns'); ?></h1>

          <div class="columns wow fadeIn">

		  <?php if(get_sub_field('number_of_columns') == '2') : ?>

			<?php
			// check if the repeater field has rows of data
			if( have_rows('column_builder') ):
			 	// loop through the rows of data
			    while ( have_rows('column_builder') ) : the_row();
			?>

          	<div class="col-2 wow fadeIn">

					<div class="circle-container">
						<div class="circlestat awards <?php echo $field['key']; ?>">
							<?php
								$image = get_sub_field('icon');
								if( !empty($image) ):
							?>

								<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

							<?php endif; ?>
						</div>
              		</div> <!-- /.circle-container -->

              		<h2>
	              		<?php if (get_sub_field('link') != '') : ?>
	              			<a href="<?php the_sub_field('link'); ?>"><?php the_sub_field('title'); ?></a>
	              		<?php else: ?>
	              			<span><?php the_sub_field('title') ?></span>
	              		<?php endif; ?>
	              	</h2>
              		<?php the_sub_field('content'); ?>

          	</div> <!-- /.col-4 -->

          	<?php endwhile; else : endif; //end of column_builder ?>

		  <?php elseif(get_sub_field('number_of_columns') == '3') : ?>

			<?php
			// check if the repeater field has rows of data
			if( have_rows('column_builder') ):
			 	// loop through the rows of data
			    while ( have_rows('column_builder') ) : the_row();
			?>

          	<div class="col-3 wow fadeIn">

					<div class="circle-container">
						<div class="circlestat awards <?php echo $field['key']; ?>">
							<?php
								$image = get_sub_field('icon');
								if( !empty($image) ):
							?>

								<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

							<?php endif; ?>
						</div>
              		</div> <!-- /.circle-container -->

              		<h2>
	              		<?php if (get_sub_field('link') != '') : ?>
	              			<a href="<?php the_sub_field('link'); ?>"><?php the_sub_field('title'); ?></a>
	              		<?php else: ?>
	              			<span><?php the_sub_field('title') ?></span>
	              		<?php endif; ?>
	              	</h2>
              		<?php the_sub_field('content'); ?>

          	</div> <!-- /.col-4 -->

          	<?php endwhile; else : endif; //end of column_builder ?>

		  <?php elseif(get_sub_field('number_of_columns') == '4') : ?>

			<?php
			// check if the repeater field has rows of data
			if( have_rows('column_builder') ):
			 	// loop through the rows of data
			    while ( have_rows('column_builder') ) : the_row();
			?>

          	<div class="col-4 wow fadeIn">

					<div class="circle-container">
						<div class="circlestat awards <?php echo $field['key']; ?>">
							<?php
								$image = get_sub_field('icon');
								if( !empty($image) ):
							?>

								<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

							<?php endif; ?>
						</div>
              		</div> <!-- /.circle-container -->

              		<h2>
	              		<?php if (get_sub_field('link') != '') : ?>
	              			<a href="<?php the_sub_field('link'); ?>"><?php the_sub_field('title'); ?></a>
	              		<?php else: ?>
	              			<?php the_sub_field('link')?>
	              			<span><?php the_sub_field('title') ?></span>
	              		<?php endif; ?>
	              	</h2>
              		<?php the_sub_field('content'); ?>

          	</div> <!-- /.col-4 -->

          	<?php endwhile; else : endif; //end of column_builder ?>

		  <?php else : endif; // end of # of columns ?>

          </div> <!-- /.columns -->

			<?php if (get_sub_field('add_button') == 'yes') : ?>
				<a class="button wow fadeInUp" href="<?php the_sub_field('button_link'); ?>"><?php the_sub_field('button_text'); ?></a>
			<?php endif; ?>

        </div> <!-- /.wrap -->
      </section> <!-- /.statistics -->
