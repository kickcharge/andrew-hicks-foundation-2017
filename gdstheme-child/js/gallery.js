jQuery(document).ready(function($){
	
	var $modal = $('#galleryModal');
	var $nextBtn = $('.next-button', $modal);
	var $prevBtn = $('.prev-button', $modal);
	var $currentThumb;
	var $modalIsClosed = true;

	function loadImageInModal() {

		removeImageFromModal();

		var imgUrl = $currentThumb.attr('href');

		var fullImg = $('<img/>').attr({ 'src' : imgUrl }).on('load', function(){

			$(this).appendTo($modal);

			if($modalIsClosed) {
				$modal.foundation('open');
			}

		});

	}

	function removeImageFromModal() {

		$('img', $modal).each(function(){
			$(this).remove();
		});

	}

	function setCurrentThumb(thumb) {

		$('.gallery-thumb').not(thumb).removeClass('active');
		$(thumb).addClass('active');
		$currentThumb = $(thumb);

	}

	function unsetActiveThumb() {
		$('.gallery-thumb').removeClass('active');
	}

	function openGalleryImage(thumb) {

		setCurrentThumb(thumb);
		loadImageInModal();

	}
	
	function nextImage() {

		if($currentThumb) {
		
			var $thumbPar = $currentThumb.parent();
			var $nextThumb = $('.gallery-thumb', $thumbPar.next());
			
			if($nextThumb.length > 0) {
				setCurrentThumb($nextThumb);
				loadImageInModal();	
			}		
					
		}
		
	}
	
	function previousImage() {

		if($currentThumb) {

			var $thumbPar = $currentThumb.parent();
			var $prevThumb = $('.gallery-thumb', $thumbPar.prev());
	
			if($prevThumb.length > 0) {
				setCurrentThumb($prevThumb);
				loadImageInModal();
			}

		}		

	}

	$('.gallery-thumb').on('click', function(e){

		e.preventDefault();

		openGalleryImage($(this));

	});

	$nextBtn.on('click', function(){

		nextImage();

	});
	
	$prevBtn.on('click', function(){

		previousImage();

	});
	
	$(document).keydown(function(e){
	    
	    if (e.keyCode == 39) { 
	       nextImage();
	       return false;
	    }	    
	    
	    if (e.keyCode == 37) { 
	       previousImage();
	       return false;
	    }	    
	    
	});	
	

	$modal.on('closed.zf.reveal', function(){

		removeImageFromModal();
		unsetActiveThumb();
		$modalIsClosed = true;

	});

	$modal.on('open.zf.reveal', function(){

		$modalIsClosed = false;

	});	
	
});