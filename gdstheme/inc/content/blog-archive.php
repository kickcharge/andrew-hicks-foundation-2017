					<?php if ( have_posts() ) : ?>

					<div class="feed">

						<div class="columns">

						<?php while ( have_posts() ) : the_post(); ?>

			              <div class="col-3">
			                <article class="post">

			                  <div class="featured-img">
			                      <div class="resource-tile-mask">
			                        <div class="yuck-Y">
			                          <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
			                        </div>
			                      </div><!-- /.resource-tile-mask -->
			                      <?php if (has_post_thumbnail() ) {
			                              the_post_thumbnail('featured-thumb');
			                            } else {
			                              echo '<img src="'.get_template_directory_uri().'/images/300x220.jpg" alt="No Thumbnail" width="300" height="220">';
			                            }
			                      ?>
			                  </div> <!-- /.featured-img -->

			                  <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

			                  <div class="post-info">
			                    <div class="date"><?php the_time('j F') ?></div> <!-- /.date -->
			                  </div> <!-- /.post-info -->

			                  <p class="post-excerpt">
				                  <?php echo excerpt(20); ?>
				              </p> <!-- /.post-excerpt -->

			                </article> <!-- /.post -->
			              </div> <!-- /.col-3 -->

						<?php endwhile; ?>

						</div><!-- /.columns -->

					</div><!-- /.feed -->

				        <?php else : ?>

				          <h2 class="no-content">No content found.</h2>

				    <?php endif; ?>