<section class="programs">  
    <div class="row title-row">
      <div class="large-12 columns">
        <h3>Programs</h3>
      </div>
    </div>
    <div class="row">
	    <?php if(get_sub_field('program')): $i = 0; ?>
		<?php while(has_sub_field('program')): $i++; ?>
			<div class="program-wrap-<?php echo $i; ?>">
				<div class="medium-11 medium-centered columns program" >
					
					<div class="row" data-equalizer>
						<div class="medium-4 columns" data-equalizer-watch>
							<div class="row">							
								<div class="large-12 columns img_link">
									<img src="<?php the_sub_field('image'); ?>" alt=""/>
									<a class="button" href="<?php the_sub_field('link'); ?>"><?php the_sub_field('link_text'); ?></a>	
								</div>
							</div>					
						</div>
						<div class="medium-8 columns" data-equalizer-watch>
							<div class="row">
								<div class="large-12 columns info">
									<h4><?php the_sub_field('program_name'); ?></h4>
									<?php the_sub_field('description'); ?>						
								</div>
							</div>
						</div>			
					</div>
				</div> 
				<i class="fa fa-chevron-down" aria-hidden="true"></i>
			</div>
				
		<?php endwhile; ?>   
		<?php endif; ?>
    
    </div>			
		
</section> <!-- /.services -->
