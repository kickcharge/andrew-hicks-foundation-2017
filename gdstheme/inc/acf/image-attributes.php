<?php

if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
	'key' => 'group_56d6676177489',
	'title' => 'Image Attributes',
	'fields' => array (
		array (
			'key' => 'field_56d667fae2de5',
			'label' => 'Featured Portfolio Image',
			'name' => 'featured_portfolio_image',
			'type' => 'true_false',
			'instructions' => 'Check this field if you\'d like the portfolio image to featured in a slideshow at the top of the page.',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'message' => '',
			'default_value' => 0,
		),
		array (
			'key' => 'field_56e2fd192fc5a',
			'label' => 'Image Positioning',
			'name' => 'image_positioning',
			'type' => 'radio',
			'instructions' => 'Use this field to optionally select the starting position of the background-image. By default, a background-image is placed at the top-left corner of an element, and repeated both vertically and horizontally. 	The first value is the horizontal position and the second value is the vertical. The top left corner is 0% 0%. The right bottom corner is 100% 100%. If you only specify one value, the other value will be 50%. . Default value is: 0% 0%',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				'left_top' => 'Left Top',
				'left_center' => 'Left Center',
				'left_bottom' => 'Left Bottom',
				'right_top' => 'Right Top',
				'right_center' => 'Right Center',
				'right_bottom' => 'Right Bottom',
				'center_top' => 'Center Top',
				'center_center' => 'Center Center',
				'center_bottom' => 'Center Bottom',
			),
			'other_choice' => 0,
			'save_other_choice' => 0,
			'default_value' => '',
			'layout' => 'horizontal',
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'attachment',
				'operator' => '==',
				'value' => 'all',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

endif;
    
?>