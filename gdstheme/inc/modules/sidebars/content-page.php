<aside class="widget">
	<?php echo do_shortcode('[gravityform id="2" title="true" description="false" ajax="true"]'); ?>
</aside><!-- /.widget -->

<aside class="widget menu-widget">
<?php
	$children = wp_list_pages(
    array(
        'child_of' => $post->ID,
        'title_li' => '',
        'echo'     => false,
				'depth'    => 1
    )
);

if ( $children ) : ?>

    <ul class="subpages">
        <h3 class="parent"><?php the_title(); ?></h3>
        <?php echo $children ?>
    </ul><!-- /.subpages -->

<?php else : ?>

	<h3 class="parent"><?php the_field('navigation_header', 'option'); ?></h3>
	<?php $sidebar_menu = get_field( 'sidebar_menu', 'option' );
  $defaults = array(
    'theme_location'  => 'sidebar_menu',
    'menu'            => $sidebar_menu,
    'container'       => 'div',
    'container_class' => '',
    'container_id'    => '',
    'menu_class'      => '',
    'menu_id'         => '',
    'echo'            => true,
    'fallback_cb'     => 'wp_page_menu',
    'before'          => '',
    'after'           => '',
    'link_before'     => '',
    'link_after'      => '',
    'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
    'depth'           => 1,
    'walker' 		  => new GDS_Walker()
  );

  wp_nav_menu( $defaults ); ?>

<?php endif; ?>

</aside> <!-- /.menu-widget -->

<?php if( get_field('call_to_action', 'option') ): ?>
  <aside class="widget text-widget">
  	<?php the_field('call_to_action', 'option'); ?>
  </aside> <!-- /.text-widget -->
<?php endif; ?>

<?php if( get_field('customer_review', 'option') ): ?>
  <aside class="widget text-widget">
    <div class="sidebar-testimonial">
  	<?php the_field('customer_review', 'option'); ?>
    </div> <!-- /.sidebar-testimonial -->
  </aside> <!-- /.testimonial-widget -->
<?php endif; ?>

<?php
$post_object = get_field('choose_your_cta');
if( $post_object ):

// override $post
$post = $post_object;
setup_postdata( $post );
?>

<aside class="widget text-widget">
<div class="sidebar-testimonial call-to-action">
    <div>
      <?php the_post_thumbnail(); ?>
    	<?php the_content(); ?>
    </div>
    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
</div><!-- /.sidebar-testimonial -->
</aside><!-- /.widget .text-widget -->
<?php endif; ?>
