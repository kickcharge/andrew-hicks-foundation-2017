<section class="event-slider">
	<div class="row title-row text-center">
      <div class="large-12 columns">
        <h3><?php echo get_sub_field('heading'); ?></h3>
      </div>
    </div>
    <div class="row column">
	    <div class="orbit event-slideshow" data-orbit>
	    	<div class="orbit-wrapper">
		    	<div class="orbit-controls">
			      <button class="orbit-previous"><span class="show-for-sr">Previous Slide</span><i class="fa fa-angle-left"></i></button>
			      <button class="orbit-next"><span class="show-for-sr">Next Slide</span><i class="fa fa-angle-right"></i></button>
			    </div>
			    <ul class="orbit-container">
				    <?php $events = get_sub_field('events'); ?>
				    <?php for($i = 0; $i < count($events); $i++){ ?>
					    <li class="<?php if($i === 0){ ?>is-active <?php } ?>orbit-slide">
					    	<div class="row small-collapse" data-equalizer>
						    	<?php if(get_the_post_thumbnail($events[$i]->ID)){ $image = true; } else { $image = false; } ?>
						    	<?php if($image){?>
							    	<div class="column small-12 medium-8 event-slider__img-wrapper" data-equalizer-watch>
								    	<?php echo get_the_post_thumbnail($events[$i]->ID, array(580, 385)); ?>
							    	</div>
						    	<?php } ?>
						    	<div class="column small-12<?php if($image){ ?> medium-4<?php } ?> event-slider__content" data-equalizer-watch>
							    	<p class="event-slider__title"><?php echo $events[$i]->post_title; ?></p>
							    	<?php
					                    $text = strip_tags($events[$i]->post_content);
				                        $words = explode(' ', $text);
										$first_30_words = array_slice($words, 0, 30);
									?>
									<p class="event-slider__excerpt"><?php echo implode(' ', $first_30_words);?>...</p>
									<a href="<?php echo $events[$i]->guid; ?>" class="event-slider__details">Event Details <i class="fa fa-angle-right"></i></a>
						    	</div>
					    	</div>
					    </li>
				    <?php } ?>				    
			    </ul>
	    	</div>
    	</div>
    </div>
    <div class="row column text-center">
	    <a href="<?php echo get_sub_field('button_link'); ?>" class="button"><?php echo get_sub_field('button_label'); ?></a>
    </div>
</section>