<section class="ways-to-help">
	<div class="row title-row">
      <div class="small-12 medium-10 large-9 small-centered columns text-center">
        <h3><?php echo get_sub_field('heading'); ?></h3>
		<p><?php echo get_sub_field('text');?></p>
      </div>
    </div>
    <div class="row small-up-2 medium-up-3">
	    <?php if(get_sub_field('ways_to_help')): $i = 0; ?>
		<?php while(has_sub_field('ways_to_help')): $i++; ?>
			<div class="way-to-help column column-block">
				<img src="<?php echo get_sub_field('image')['sizes']['featured-thumb'] ?>" alt="<?php echo get_sub_field('image')['alt']; ?>">
				<div class="way-to-help__content">
					<p class="way-to-help__title"><?php echo get_sub_field('title'); ?></p>
					<?php echo get_sub_field('description'); ?>
					<a href="<?php if(get_sub_field('button_url')){ echo get_sub_field('button_url'); } else { echo '#'; } ?>" class="button"><?php echo get_sub_field('button_label'); ?></a>
				</div>
			</div>
		<?php endwhile; ?>   
		<?php endif; ?>
    </div>			
</section>