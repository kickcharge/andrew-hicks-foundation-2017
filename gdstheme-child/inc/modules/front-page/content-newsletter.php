<section class="newsletter">
	<div class="row">
		<div class="columns show-for-medium medium-4">
			<img src="<?php echo get_sub_field('icon')['url']; ?>" alt="<?php echo get_sub_field('icon')['alt']; ?>">
		</div>
		<div class="columns small-12 medium-8">
		    <h3><?php echo get_sub_field('title'); ?></h3>
		    <?php echo get_sub_field('text'); ?>
		</div>
	</div>
</section>