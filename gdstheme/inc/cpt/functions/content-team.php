	<div class="team-container">
		
		<div class="intro-box">
			
			<div class="intro-content">
				
				<?php the_field('team_section_content', 'option'); ?>
				
			</div><!-- /.intro-content -->			
			
		</div><!-- /.intro-box -->

		<?php
			$posts = new WP_Query(array(
				'post_type' => 'team',
				'posts_per_page' => -1,
				'orderby' => 'menu_order',
				'order' => 'ASC'
				)
			);
			while ( $posts->have_posts() ) : $posts->the_post();
		?>

	      	<div id="post-<?php the_ID(); ?>" <?php post_class('bio-profile team-member'); ?>>
				<?php
					$primary_image = get_field('primary_image');
					$secondary_image = get_field('secondary_image');
					
					if( !empty($primary_image) || ($secondary_image) ):
					
					// vars
					$primary_url = $primary_image['url'];
					$primary_title = $primary_image['title'];
					$primary_alt = $primary_image['alt'];
					$primary_caption = $primary_image['caption'];
				
					// thumbnail
					$primary_size = 'large';
					$primary_thumb = $primary_image['sizes'][ $primary_size ];
					$primary_width = $primary_image['sizes'][ $primary_size . '-width' ];
					$primary_height = $primary_image['sizes'][ $primary_size . '-height' ];
					
					// vars
					$secondary_url = $secondary_image['url'];
					$secondary_title = $secondary_image['title'];
					$secondary_alt = $secondary_image['alt'];
					$secondary_caption = $secondary_image['caption'];
				
					// thumbnail
					$secondary_size = 'large';
					$secondary_thumb = $secondary_image['sizes'][ $secondary_size ];
					$secondary_width = $secondary_image['sizes'][ $secondary_size . '-width' ];
					$secondary_height = $secondary_image['sizes'][ $secondary_size . '-height' ];
					
				?>
		  					  		
		  		<figure>
		  			<img width="<?php echo $primary_width; ?>" height="<?php echo $primary_height; ?>" data-primary="<?php echo $primary_thumb; ?>" data-secondary="<?php echo $secondary_thumb; ?>" src="<?php echo $primary_thumb; ?>" />
			  	<?php endif; ?>
		  			<figcaption>
		  				<p class="team-member-name"><?php the_title(); ?></p>
		  				<p class="team-member-title"><?php the_field('job_title'); ?></p>
		  				<a class="bio-btn-more bio-btn" href="#">
			  				<i class="fa fa-angle-up"></i>
		  				</a>
		  				<a class="bio-btn-less bio-btn" href="#">
			  				<i class="fa fa-angle-down"></i>
		  				</a>
		  				<p class="team-member-bio">
			  				<?php echo excerpt(25); ?>
			  				<a class="bio-link" href="<?php the_permalink(); ?>">Read More</a>
			  			</p>
		  			</figcaption>
		  		</figure>
			  		
	      	</div><!-- /.team-member -->

		<?php endwhile; wp_reset_postdata(); ?>

	</div><!-- /.testimonial-container -->