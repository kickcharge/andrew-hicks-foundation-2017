    <?php get_header(); ?>

	<?php if(get_field('gallery_placement') == 'header') { ?>

	<section id="portfolio_gallery">

		<?php

		$images = get_field('image_gallery');

		if( $images ): ?>
		    <div class="portfolio-slider">
		            <?php foreach( $images as $image ): ?>
		                <div>
		                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" width="<?php echo $image['width']; ?>" />
		                </div>
		            <?php endforeach; ?>
		    </div><!-- /.portfolio-slider -->
		<?php endif; ?>

	</section><!-- /#portfolio_gallery -->

	<?php } elseif(get_field('gallery_placement') == 'content') { ?>

	<!-- Do Nothing -->

	<?php } ?>

	<?php if(get_field('gallery_placement') == 'header') { ?>
    	<section class="container">
	<?php } elseif(get_field('gallery_placement') == 'content') { ?>
		<section class="container content_lightbox">
	<?php } ?>
        <div class="wrap">

          <article id="content">

		  	<?php if ( have_posts() ) { while ( have_posts() ) { the_post(); ?>

			  	<h2><?php the_title(); ?></h2>

			  	<?php the_content()?>

            <?php } ?>

            <?php } else { ?>

              <h1>No content found.</h1>

            <?php } ?>

            <h2>Testimonial</h2>

            <?php the_field('customer_review'); ?>

            <p>
	            <?php the_field('customer_name'); ?><br />
	            <?php the_field('company_name'); ?><br />
	            <?php the_field('customer_location'); ?>
            </p>

            <?php if(get_field('gallery_placement') == 'content') { ?>

			<div class="content_gallery">

				<h2>Photo Gallery</h2>

				<?php
					$images = get_field('image_gallery');
					if( $images ):
				?>
				    <ul>
				        <?php foreach( $images as $image ): ?>
				            <li>
				                <a title="<?php echo $image['caption']; ?>" rel="group" class="fancybox" href="<?php echo $image['url']; ?>">
				                     <img title="<?php echo $image['caption']; ?>" src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
				                </a>
				            </li>
				        <?php endforeach; ?>
				    </ul>
				<?php endif; ?>

			</div><!-- /.content_gallery -->

			<?php } elseif(get_field('gallery_placement') == 'header') { ?>

				<!-- Do Nothing -->

			<?php } ?>

	          <div class="more_galleries clearfix">

		          <h2>More Case Studies</h2>

		          <ul class="galleries">

					<?php
						// get posts

						$categories = get_the_terms($post->ID, 'service-provided');
						if ($categories) { foreach($categories as $cat); } //echo $cat->slug .' ';

						$args = new WP_Query(array(
							'post_type' => 'case-studies',
							'posts_per_page' => 4,
							'post_status' => 'publish',
							'post_parent' => null,
							'tax_query' => array(
								array(
									'taxonomy' => 'service-provided',
									'field' => 'slug',
									'terms' => ($cat->slug .' ')
								),
							),
							'order'	=> 'DESC'
						));

						while ( $args->have_posts() ) : $args->the_post();

					?>

			          <li class="gallery_item" data-category="<?php $posttags = get_the_terms($post->ID, 'service'); if ($posttags) { foreach($posttags as $tag) { echo str_replace('-','_',$tag->slug . ' '); } } ?>">
				         <a href="<?php echo the_permalink(); ?>">
				          <?php
					          if ( has_post_thumbnail() ) {
						        //Show Featured Image
					          	the_post_thumbnail(array(237, 163));
							  } else {
				            	// Do Nothing
				            }
				          ?>
				          <div class="info">
					          <?php the_title(); ?>
				          </div><!-- /.info -->
				         </a>
			          </li><!-- /.gallery_item -->

			        <?php endwhile; wp_reset_postdata(); ?>

		          </ul><!-- /.galleries -->

	          </div><!-- /.more_galleries -->

          </article><!-- /#content -->

          <?php get_sidebar( 'right' ); ?>

        </div> <!-- /.wrap -->
      </section> <!-- /.container -->

      <?php get_footer(); ?>