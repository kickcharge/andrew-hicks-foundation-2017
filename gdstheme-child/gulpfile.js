var gulp = require('gulp');
var $    = require('gulp-load-plugins')();

var sassPaths = [
  'bower_components/foundation-sites/scss',
  'bower_components/motion-ui/src',
  'bower_components/gravity-forms-sass',
  'bower_components/font-awesome/scss'
];

gulp.task('sass', function() {
  gulp.src('scss/styles.scss')
      .pipe($.sourcemaps.init())
      .pipe($.sass({
        includePaths: sassPaths
      }))
      .pipe($.autoprefixer({
        browsers: ['last 2 versions', 'ie >= 9']
      }))
      .pipe($.sourcemaps.write('./'))
      .pipe(gulp.dest('public'));
});

gulp.task('default', ['sass'], function() {
  gulp.watch(['scss/**/*.scss'], ['sass']);
});
