<?php

	if ( ! function_exists( 'service_provided' ) ) {

	// Register Custom Taxonomy
	function service_provided() {

		$labels = array(
			'name'                       => _x( 'Services Provided', 'Taxonomy General Name', 'gdstheme' ),
			'singular_name'              => _x( 'Service Provided', 'Taxonomy Singular Name', 'gdstheme' ),
			'menu_name'                  => __( 'Services Provided', 'gdstheme' ),
			'all_items'                  => __( 'All Services', 'gdstheme' ),
			'parent_item'                => __( 'Parent Item', 'gdstheme' ),
			'parent_item_colon'          => __( 'Parent Item:', 'gdstheme' ),
			'new_item_name'              => __( 'New Service', 'gdstheme' ),
			'add_new_item'               => __( 'Add New Service', 'gdstheme' ),
			'edit_item'                  => __( 'Edit Service', 'gdstheme' ),
			'update_item'                => __( 'Update Service Provided', 'gdstheme' ),
			'view_item'                  => __( 'View Service', 'gdstheme' ),
			'separate_items_with_commas' => __( 'Separate items with commas', 'gdstheme' ),
			'add_or_remove_items'        => __( 'Add or remove items', 'gdstheme' ),
			'choose_from_most_used'      => __( 'Choose from the most used', 'gdstheme' ),
			'popular_items'              => __( 'Popular Items', 'gdstheme' ),
			'search_items'               => __( 'Search Items', 'gdstheme' ),
			'not_found'                  => __( 'Not Found', 'gdstheme' ),
		);
		$args = array(
			'labels'                     => $labels,
			'hierarchical'               => true,
			'public'                     => true,
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => false,
		);
		register_taxonomy( 'service-provided', array( 'case-studies' ), $args );

	}

	// Hook into the 'init' action
	add_action( 'init', 'service_provided', 0 );

	}

?>